float cross2d(final PVector v1, final PVector v2) { //<>//
  return v1.x * v2.y - v1.y * v2.x;
}

enum Color {
  Red(0), 
    Green(1), 
    Blue(2);

  final int id;

  private Color(int id) {
    this.id = id;
  }
}

color toColorCode(Color c) {
  switch(c) {
  case Red:
    return RED;
  case Green:
    return GREEN;
  case Blue:
    return BLUE;
  }
  throw new IllegalArgumentException();
}

final class Point implements Drawable {
  static final int RADIUS = 10;

  final int x;
  final int y;
  Color c;
  boolean cameraLocated = false;

  Point(final int x, final int y) {
    this.x = x;
    this.y = y;
  }

  PVector toPVector() {
    return new PVector(this.x, this.y);
  }

  void draw() {
    PShape circle = createShape(ELLIPSE, this.x, this.y, Point.RADIUS, Point.RADIUS);

    if (this.cameraLocated) {
      tint(toColorCode(this.c));
      image(camera, this.x, this.y, 30, 30);
    } else {
      if (this.c != null) {
        circle.setFill(toColorCode(this.c));
      } else {
        circle.setFill(GRAY);
      }

      shape(circle);
    }
  }
}

final class Edge implements Drawable {
  final Point left;
  final Point right;

  Edge(final Point left, final Point right) {
    this.left = left;
    this.right = right;
  }

  PVector toPVector() {
    return new PVector(this.right.x - this.left.x, this.right.y - this.left.y);
  }

  boolean hasPoint(final Point p) {
    if (this.left == p || this.right == p) {
      return true;
    } else {
      return false;
    }
  }

  boolean doesCollide(final Edge other) {
    final PVector edge1 = this.toPVector();
    final PVector edge2 = other.toPVector();

    final PVector v = other.left.toPVector().sub(this.left.toPVector());
    final float crossEdge1Edge2 = cross2d(edge1, edge2);

    if (crossEdge1Edge2 == 0) {
      return false;
    }

    final float crossVEdge1 = cross2d(v, edge1);
    final float crossVEdge2 = cross2d(v, edge2);

    final float t1 = crossVEdge2 / crossEdge1Edge2;
    final float t2 = crossVEdge1 / crossEdge1Edge2;

    if (t1 < 0 || t1 > 1 || t2 < 0 || t2 > 1) {
      return false;
    } else {
      return true;
    }
  }

  Edge flip() {
    return new Edge(this.right, this.left);
  }

  void draw() {
    line(this.left.x, this.left.y, this.right.x, this.right.y);
  }
}

final class IlligalPolygonException extends RuntimeException {
}

final class PolygonBuilder extends Polygon {
  PolygonBuilder() {
    super(new ArrayList(), new ArrayList());
  }

  void addPoint(Point newPoint) {
    final int pointBufSize = points.size();
    if (pointBufSize >= 1) {
      final Edge newEdge = new Edge(this.points.get(pointBufSize - 1), newPoint);
      for (int i = 0; i < this.edges.size() - 1; i++) {
        if (newEdge.doesCollide(this.edges.get(i))) {
          return;
        }
      }
      this.edges.add(newEdge);
    }
    this.points.add(newPoint);
  }

  Polygon build() {
    if (!this.canBuild()) {
      throw new IlligalPolygonException();
    }
    final Edge newEdge = new Edge(this.points.get(this.points.size() - 1), this.points.get(0));
    this.edges.add(newEdge);
    return new Polygon((ArrayList<Point>)this.points.clone(), (ArrayList<Edge>)this.edges.clone());
  }

  boolean canBuild() {
    if (this.points.size() < 3) {
      return false;
    }
    final Edge newEdge = new Edge(this.points.get(this.points.size() - 1), this.points.get(0));
    for (int i = 1; i < this.edges.size() - 1; i++) {
      if (newEdge.doesCollide(this.edges.get(i))) {
        return false;
      }
    }
    return true;
  }

  Point getLastPoint() {
    return this.points.get(this.points.size() - 1);
  }

  void draw() {
    if (!this.points.isEmpty()) {
      Point last = this.getLastPoint();

      PShape line = createShape(LINE, last.x, last.y, mouseX, mouseY);

      color c = color(FG, 100);
      if (!this.readyToBuild()) {
        c = color(RED, 100);
      }

      line.setStroke(c);
      shape(line);
    }
    super.draw();
  }

  // マウスが最初の点の上にのっていたてかつBuild可能なときTrue
  boolean readyToBuild() {
    Edge pointToCursor = new Edge(this.getLastPoint(), new Point(mouseX, mouseY));
    if(!this.canBuild() && mouseIsOverrapFirstPoint()) {
      return false;
    }
    if (!this.edges.isEmpty()) {
      for (int i = 0; i < this.edges.size() - 1; i++) {
        if (pointToCursor.doesCollide(this.edges.get(i))) {
          if(!mouseIsOverrapFirstPoint()) {
            return false;
          }
        }
      }
    }
    return true;
  }
}

final class NoSuchPointException extends RuntimeException {
}

class Polygon implements Drawable {
  final ArrayList<Point> points;
  final ArrayList<Edge> edges;

  Polygon(final ArrayList<Point> points, final ArrayList<Edge> edges) {
    this.points = points;
    this.edges = edges;
  }

  void draw() {
    for (Edge edge : this.edges) {
      edge.draw();
    }
    for (Point point : this.points) {
      point.draw();
    }
  }

  boolean isTriangle() {
    return edges.size() == 3;
  }

  Point furthest() {
    Point further = this.points.get(0);

    for (Point p : this.points) {
      if (p.toPVector().mag() > further.toPVector().mag()) {
        further = p;
      }
    }

    return further;
  }

  Edge[] neighborEdge(Point p) {
    Edge[] result = {null, null};

    for (Edge e : this.edges) {
      if (e.left == p) {
        result[1] = e;
      } else if (e.right == p) {
        result[0] = e;
      }
    }

    assert((result[0] != null) == (result[1] != null));

    return result;
  }

  boolean doesTriangleOverrapsAnyPoints(Triangle t) {
    for (Point p : this.points) {
      if (t.hasPoint(p)) {
        continue;
      }
      if (t.doesPointCollide(p)) {
        return true;
      }
    }

    return false;
  }

  void deletePoint(Point p) {
    Edge[] affect = this.neighborEdge(p);

    this.edges.remove(this.edges.indexOf(affect[0]));
    this.edges.remove(this.edges.indexOf(affect[1]));
    this.points.remove(this.points.indexOf(p));
  }

  boolean isCycle() {
    for (int i = 0; i < this.edges.size() - 1; i++) {
      if (this.edges.get(i).right != this.edges.get(i + 1).left) {
        return false;
      }
    }
    if (this.edges.get(this.edges.size() - 1).right != this.edges.get(0).left) {
      return false;
    }

    return true;
  }
}


class PolygonTriangulator {
  final Polygon target;
  final Polygon work;
  final ArrayList<Triangle> triangles = new ArrayList();
  Float dir = null;

  PolygonTriangulator(Polygon target) {
    this.target = target;
    this.work = new Polygon((ArrayList)this.target.points.clone(), (ArrayList)this.target.edges.clone());
  }

  boolean isInward(Edge[] neighbors) {
    assert(neighbors.length == 2);

    PVector left = new Edge(neighbors[0].right, neighbors[0].left).toPVector();
    PVector right = new Edge(neighbors[1].left, neighbors[1].right).toPVector();

    if (this.dir == null) {
      this.dir = cross2d(left, right);
      return true;
    } else {
      float ndir = cross2d(left, right);
      // 同符号か？
      return (ndir >= 0) == (this.dir >= 0);
    }
  }

  void step() {
    Point workPoint = this.work.furthest();
    Edge[] neighbors = this.work.neighborEdge(workPoint);

    Edge triangularEdge;

    while (true) {
      // 三角形が外側を向いていないか
      if (!this.isInward(neighbors)) {
        workPoint = neighbors[1].right;
        neighbors = this.work.neighborEdge(workPoint);
        continue;
      }

      triangularEdge = new Edge(neighbors[1].right, neighbors[0].left);

      Triangle t = new Triangle(
        new Point[]{neighbors[0].left, workPoint, neighbors[1].right}, 
        new Edge[]{neighbors[0], neighbors[1], triangularEdge}
        );

      // 三角形内に点が無いか
      if (this.work.doesTriangleOverrapsAnyPoints(t)) {
        workPoint = neighbors[1].right;
        neighbors = this.work.neighborEdge(workPoint);
        continue;
      }

      this.triangles.add(t);

      break;
    }

    this.target.edges.add(triangularEdge.flip());
    this.work.edges.add(triangularEdge.flip());

    // 点消去
    this.work.deletePoint(workPoint);
  }

  TriangulatedPolygon triangulate() {
    while (!this.work.isTriangle()) {
      this.step();
    }
    Triangle t = new Triangle(
      new Point[]{this.work.points.get(0), this.work.points.get(1), this.work.points.get(2)}, 
      new Edge[]{this.work.edges.get(0), this.work.edges.get(1), this.work.edges.get(2)}
      );
    this.triangles.add(t);
    return new TriangulatedPolygon(this.target, this.triangles);
  }
}

class TriangulatedPolygon extends Polygon {
  final ArrayList<Triangle> triangles;

  TriangulatedPolygon(Polygon polygon, ArrayList<Triangle> triangles) {
    super(polygon.points, polygon.edges);
    this.triangles = triangles;
  }
}

class Triangle {
  final Point[] points;
  final Edge[] edges;

  Triangle(Point[] points, Edge[] edges) {
    if (points.length != 3 || edges.length != 3) {
      throw new IllegalArgumentException();
    }

    this.points = points;
    this.edges = edges;
  }

  boolean hasPoint(Point p) {
    for (Point i : this.points) {
      if (i == p) {
        return true;
      }
    }

    return false;
  }

  boolean isCycle() {
    return this.edges[0].right == this.edges[1].left &&
      this.edges[1].right == this.edges[2].left &&
      this.edges[2].right == this.edges[0].left;
  }

  boolean doesPointCollide(Point p) {
    assert(this.isCycle());

    ArrayList<Float> crosses = new ArrayList();

    for (Edge e : this.edges) {
      PVector v1 = e.toPVector();
      PVector v2 = new Edge(e.left, p).toPVector();

      crosses.add(cross2d(v1, v2));
    }

    for (int i = 0; i < crosses.size() - 1; i++) {
      if ((crosses.get(i) >= 0) != (crosses.get(i + 1) >= 0)) {
        return false;
      }
    }

    return true;
  }
}

class PolygonPainter {
  final ArrayList<Triangle> triangles;

  PolygonPainter(ArrayList<Triangle> triangles) {
    this.triangles = triangles;
  }

  Color otherColor(Color a, Color b) {
    ArrayList<Color> cs = new ArrayList();
    cs.add(a);
    cs.add(b);

    if (cs.indexOf(Color.Red) == -1) {
      return Color.Red;
    } else if (cs.indexOf(Color.Green) == -1) {
      return Color.Green;
    } else {
      return Color.Blue;
    }
  }

  boolean singleTrianglePaint(Triangle t) {
    // TODO 2つ彩色されていたら残りの一つも彩色
    // 成功ならtrue 失敗ならfalseを返す

    Color[] colors = {t.points[0].c, t.points[1].c, t.points[2].c};

    int colored = 3;
    for (Color c : colors) {
      if (c == null) {
        colored--;
      }
    }
    if (colored <= 1) {
      return false;
    }

    Color[] paintedColors = new Color[2];
    int arrptr = 0;
    int nullIndex = 0;
    for (int i = 0; i < 3; i++) {
      if (colors[i] != null) {
        paintedColors[arrptr] = colors[i];
        arrptr++;
      } else {
        nullIndex = i;
      }
    }
    t.points[nullIndex].c = this.otherColor(paintedColors[0], paintedColors[1]);

    return true;
  }

  void paint() {
    Triangle first = this.triangles.get(0);
    first.points[0].c = Color.Red;
    first.points[1].c = Color.Green;

    while (!this.triangles.isEmpty()) {
      for (int i = 0; i < this.triangles.size(); i++) {
        if (this.singleTrianglePaint(this.triangles.get(i))) {
          this.triangles.remove(i);
        }
      }
    }
  }
}

class CameraLocater {
  final ArrayList<Point> points;

  CameraLocater(ArrayList<Point> points) {
    this.points = points;
  }

  Color putCamera() {
    Color[] colors = {Color.Red, Color.Green, Color.Blue};
    int[] colorsNum = {0, 0, 0};
    for (Point p : this.points) {
      colorsNum[p.c.id]++;
    }

    int minIndex = 0;
    for (int i = 0; i < 3; i++) {
      if (colorsNum[minIndex] > colorsNum[i]) {
        minIndex = i;
      }
    }

    for (Point p : this.points) {
      if (p.c == colors[minIndex]) {
        p.cameraLocated = true;
      }
    }

    return colors[minIndex];
  }
}

interface Info {
  String getInfo();
  color getBackground();
  color getTextColor();
}

class NumInfo implements Info {
  static final int WIDTH = 160;
  static final int HEIGHT = 480 / 6;

  final color background;
  final String label;
  int num;
  color textColor = color(BG);

  NumInfo(color background, String label, int num) {
    this.background = background;
    this.label = label;
    this.num = num;
  }

  String getInfo() {
    return this.label + String.valueOf(this.num);
  }

  color getBackground() {
    return this.background;
  }

  color getTextColor() {
    return this.textColor;
  }
}

enum ButtonState {
    Delete, 
    Back,
    GoNext,
}

class Button implements Info {
  ButtonState bs = ButtonState.Delete;

  void update() {
    if (state == State.Input) {
      this.bs = ButtonState.Delete;
    } else if (state == State.Output) {
      this.bs = ButtonState.Back;
    } else {
      this.bs = ButtonState.GoNext;
    }
  }

  String getInfo() {
    switch(this.bs) {
    case Delete:
      return "やり直し";
    case Back:
      return "最初から";
    case GoNext:
      return "次へ";
    }
    return "";
  }

  color getBackground() {
    switch(this.bs) {
    case Delete:
      return DARKRED; 
    case Back:
      return AQUA;
    case GoNext:
      return YELLOW;
    }
    throw new IllegalArgumentException();
  }

  color getTextColor() {
    return color(BG);
  }
}

interface Drawable {
  void draw();
}

enum State {
  Input, 
    Triangulate,
    Paint,
    Camera,
    Output,
}

final color BG = color(0xfb, 0xf1, 0xc7);
final color FG = color(0x3c, 0x38, 0x36);
final color GRAY = color(0x7c, 0x6f, 0x64);
final color LIGHTGRAY = color(0xbd, 0xae, 0x93);
final color RED = color(0xcc, 0x24, 0x21);
final color DARKRED = color(0x9d, 0x00, 0x06);
final color GREEN = color(0x98, 0x97, 0x1a);
final color BLUE = color(0x45, 0x85, 0x88);
final color AQUA = color(0x68, 0x9d, 0x6a);
final color YELLOW = color(0xd7, 0x99, 0x21);

PolygonBuilder polygonBuilder = new PolygonBuilder();
Polygon polygon;
PolygonTriangulator pt;
TriangulatedPolygon tp;
ArrayList<Info> infos = new ArrayList();
PImage camera;
PFont font;

State state = State.Input;

void drawInfo() {
  textSize(26);
  int heightPerInfo = 480 / infos.size();
  for (int i = 0; i < infos.size(); i++) {
    PShape back = createShape(RECT, 640, heightPerInfo * i, 160, heightPerInfo);
    back.setFill(infos.get(i).getBackground());
    back.setStrokeWeight(0);
    shape(back);
    fill(infos.get(i).getTextColor());
    text(infos.get(i).getInfo(), 15 + 640, heightPerInfo * i + heightPerInfo / 2 - 15, 160, heightPerInfo);
  }
  PShape back = createShape(RECT, 640, 0, 5, 480);
  back.setFill(color(FG, 60));
  back.setStrokeWeight(0);
  shape(back);
}

// 画面下部の説明を表示
void showInstruction(String text) {
  textSize(30);
  fill(BG);
  text(text, 10, 510);
}

// フォントの初期化と設定
void applyFont() {
  font = createFont("Source Han Sans JP", 30);
  textFont(font);
}

void setup() {
  size(800, 520);
  ellipseMode(RADIUS);
  background(BG);
  smooth(4);
  imageMode(CENTER);
  stroke(FG);
  strokeWeight(3);
  textSize(26);
  applyFont();

  camera = loadImage("camera.png");
  infos.add(new NumInfo(GRAY, "Point:", 0));
  infos.add(new NumInfo(toColorCode(Color.Red), "Red:", 0));
  infos.add(new NumInfo(toColorCode(Color.Green), "Green:", 0));
  infos.add(new NumInfo(toColorCode(Color.Blue), "Blue:", 0));
  infos.add(new NumInfo(LIGHTGRAY, "Camera:", 0));
  infos.add(new Button());
}

// マウスの右下にポップを表示
void showPopBase(String text, color c) {
  final int alpha = 160;
  fill(FG, alpha);
  final int gap = 10;
  final int popWidth = 200;
  final int popHeight = 20;
  rect(mouseX + gap, mouseY + gap, popWidth, popHeight);
  fill(c);
  final int offsetX = 3;
  final int offsetY = 15;
  textSize(15);
  text(text, mouseX + gap + offsetX, mouseY + gap + offsetY);
}

// マウスの右下にポップを表示
void showPopError(String text) {
  showPopBase(text, RED);
}

// マウスの右下にポップを表示
void showPop(String text) {
  showPopBase(text, BG);
}

void draw() {
  background(BG);
  ((Button)infos.get(5)).update();
  fill(FG);
  rect(0, 480, 800, 40);
  switch(state) {
  case Input:
    ((NumInfo)infos.get(0)).num = polygonBuilder.points.size();
    polygonBuilder.draw();
    showInstruction("多角形を入力してください クリックで頂点を配置");
    if(!polygonBuilder.points.isEmpty() && polygonBuilder.readyToBuild() && mouseIsOverrapFirstPoint()) {
      showPop("クリックで決定");
    } else if(!polygonBuilder.points.isEmpty() && polygonBuilder.points.size() < 3 && !polygonBuilder.readyToBuild()) {
      showPopError("頂点は3つ以上必要です");
    } else if(!polygonBuilder.points.isEmpty() && !polygonBuilder.readyToBuild()) {
      showPopError("各辺が交差してはいけません");
    }
    break;
  case Triangulate:
    polygon.draw();
    showInstruction("多角形を三角形に分割します");
    break;
  case Paint:
    tp.draw();
    showInstruction("各頂点を同じ色が隣り合わないように色を塗ります");
    break;
  case Camera:
    tp.draw();
    showInstruction("最後に、一番少ない色にカメラを設置します");
    break;
  case Output:
    tp.draw();
    showInstruction("「最初から」を押すと最初からやりなおせます");
    break;
  }
  drawInfo();
}

void clear() {
  polygonBuilder = null;
  polygonBuilder = new PolygonBuilder();
  ((NumInfo)infos.get(1)).num = 0;
  ((NumInfo)infos.get(2)).num = 0;
  ((NumInfo)infos.get(3)).num = 0;
  ((NumInfo)infos.get(4)).num = 0;
  ((NumInfo)infos.get(4)).textColor = BG;
  state = State.Input;
}

boolean mouseIsOverrapFirstPoint() {
  if(!polygonBuilder.points.isEmpty()) {
    int x = mouseX - polygonBuilder.points.get(0).x;
    int y = mouseY - polygonBuilder.points.get(0).y;
    int r = Point.RADIUS;
    return x * x + y * y <= r * r;
  }
  
  return false;
}

void mouseClicked() {
  if (mouseButton == LEFT) {
    switch(state) {
      case Input:
        if(mouseIsOverrapFirstPoint()) {
          try {
            polygon = polygonBuilder.build();
          } 
          catch(IlligalPolygonException _e) {
            println("Error!");
            return;
          }
          state = State.Triangulate;
          return;
        }
        if (mouseX <= 640 - Point.RADIUS) {
          polygonBuilder.addPoint(new Point(mouseX, mouseY));
        }
        if (mouseX >= 640 && mouseY >= 480 / 6 * 5) {
          clear();
        }
        break;
      case Triangulate:
        pt = new PolygonTriangulator(polygon);
        tp = pt.triangulate();
        if (mouseX >= 640 && mouseY >= 480 / 6 * 5) {
          state = State.Paint;
        }
        break;
      case Paint:
        PolygonPainter pp = new PolygonPainter(tp.triangles);
        pp.paint();
        if (mouseX >= 640 && mouseY >= 480 / 6 * 5) {
          state = State.Camera;
        }
        for (Point p : tp.points) {
          ((NumInfo)infos.get(p.c.id + 1)).num++; 
        }
        break;
      case Camera:
        CameraLocater cl = new CameraLocater(tp.points);
        Color cameraColor = cl.putCamera();
        for (Point p : tp.points) {
          if (p.cameraLocated) {
            ((NumInfo)infos.get(4)).num++;
            ((NumInfo)infos.get(4)).textColor = toColorCode(cameraColor);
          }
        }
        if (mouseX >= 640 && mouseY >= 480 / 6 * 5) {
          state = State.Output;
        }
        break;
      case Output:
        if (mouseX >= 640 && mouseY >= 480 / 6 * 5) {
          clear();
        }
        break;
    }
  }
}